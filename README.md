# BattleMaster BLR-1E with campaign start

This mod adds the apocryphal BattleMaster variant BLR-1E to Mechwarrior 5. 

*The BLR-1E was designed to excel in the brutal, close quarters urban combat of the Amaris Civil War. To achieve this goal, engineers added additional SRMs to the original Battlemaster's already fiersome array of short range weapons. A second PPC was mounted in the left arm. Beagle Active Probe and Guardian ECM gave the 1E's pilot an edge in the information war. To free up weight for the extra weapons and equipment, the 1E was built with an XL engine and endosteel chassis. Unfortunately the BLR-1E didn't reach production in time to fulfill it's intended purpose. SLDF Royal units began taking delivery of the new mech three weeks after the capture of Stefan Amaris. All 1Es are believed to have left the Inner Sphere during Kerensky's exodus.*

The BLR-1E is available in the following ways:

* Included in your starting lance when beginning a new campaign, but only if you skip the tutorial.
* Instant action
* Stores (haven't found it yet myself, so this is unconfirmed)
* Very small probability that the random drop decks generated for the Comstar faction can include this mech.

## Using this mod in Mechwarrior 5

To use this mod download it from [Nexus Mods](https://www.nexusmods.com/mechwarrior5mercenaries/mods/355?tab=description).

## Using these files in your own mod

Feel free to use these files in your own Mechwarrior 5 mods! Please give credit to the author `moddingisthegame`.

1. Purchase [Mechwarrior 5 for PC](https://www.epicgames.com/store/en-US/p/mechwarrior-5)
2. Install the [Mechwarrior 5 Modding Toolkt](https://www.epicgames.com/store/en-US/p/mechwarrior-5--mod-editor)
3. Install [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
4. Navigate to the Mechwarrior 5 Editor Plugins directory (typically C:\Program Files\Epic Games\MechWarrior5Editor\MW5Mercs\Plugins) in your terminal of choice
5. `git clone https://gitlab.com/mechwarrior5mods/mechs/battlemasterblr1ewithcampaignstart.git`
6. Launch the Mechwarrior 5 Modding Toolkit.
7. Click the dropdown next to Manage Mod and select `battlemasterblr1ewithcampaignstart`
8. Mod away!

## Compatibility

* This mod overrides the Battlemaster skins, so will conflict with other mods that add new variants of the Battlemaster.
* May conflict with other mods that alter the official Battlemaster variants.
* May conflict with other mods that alter the no tutorial campaign start.